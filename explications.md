# 1 . Exercice

# 1.1 Prise en main

Au début nous devons faire la mise en place de l'arbre permettant de connecter les différentes branches entre elles avec comme point commun la table home.

Ensuite, nous avons connecter les diiférentes branches et en accord avec le formateur, j'ai continué le modèle de données avec les adresses url pour prendre les données dont j'ai besoin pour faire l'exercice.

![alt text](Modèle.png)

# 2.1 Exercice des URL

Maintenant nous devons prendre les URL et le principe des pages.

[Home](https://www.paranormaldatabase.com/)

[England](https://www.paranormaldatabase.com/regions/southeast.html)

[Berkshire](https://www.paranormaldatabase.com/berkshire/berkdata.php)
[Buckinghamshire](https://www.paranormaldatabase.com/buckinghamshire/buckdata.php)

Ce sont des exemples de lien aveccomme structures: le nom du compté puis le raccourci du comté et le terme data.

[Scotland](https://www.paranormaldatabase.com/regions/scotland.html)
[Structure des Islands](https://www.paranormaldatabase.com/islands/orcndata.php)
[Structure des Lowlands](https://www.paranormaldatabase.com/lowlands/Angudata.php)
[Structure des places of notes](https://www.paranormaldatabase.com/hotspots/aberdeen.php)
[Le hotspots pour les highlands](https://www.paranormaldatabase.com/hotspots/aberdeen.php)
[Wales](https://www.paranormaldatabase.com/regions/wales.html)
[Wales et exemples](https://www.paranormaldatabase.com/wales/clwyd.php)
[Northern Ireland](https://www.paranormaldatabase.com/regions/northernireland.html)
[Structure de Northern Ireland](https://www.paranormaldatabase.com/ireland/antrim.php)
[Ireland](https://www.paranormaldatabase.com/regions/ireland.html)
[Structure de Ireland](https://www.paranormaldatabase.com/ireland/carlow.php)

## 2.2 Lecture des URL

Les URL pour le pays Ireland sont simples: Le nom du pays Ireland puis le comté et le nom du fichier en l'occurence PHP.

Pour le pays England nous avons: le nom du pays, le nom du comté et le nom du comté en raccourci avec le nom data par exemple pour Hampshire ce sera hampdata.php
[Hampshire](https://www.paranormaldatabase.com/hampshire/hampdata.php)
Pour chaque ville il y a ce terme qui revient pour England et Scotland: [Aberdeen](https://www.paranormaldatabase.com/hotspots/aberdeen.php)
[Brighton](https://www.paranormaldatabase.com/hotspots/brighton.php)

Le nom de la ville est écrit intégralement.
Pour le mot somedata: cela vient du comté de somerset and Bristol et ce qui est important à noter c'est que les quatre premières lettres du comté sont utilisées pour la construction du lien et juste en attaché data.

## 2.3 Remarques du fichier

Tout d'abord les liens ne suivent pas les mêmes structures en fonction de où nous cliquons: par exemple, nous avons plus des fichiers avec des liens comme norfolk/norpages/norfdata.php ou encore greatermanchester/index.php, https://www.paranormaldatabase.com/reports/underground.php, https://www.paranormaldatabase.com/london/lonpages/londdata.php.

Il existe d'autres liens comme: [Greater Manchester](https://www.paranormaldatabase.com/greatermanchester/index.php)

[Greater London](https://www.paranormaldatabase.com/london/lonpages/londdata.php)

[Underground](https://www.paranormaldatabase.com/reports/underground.php)

[Merseyside](https://www.paranormaldatabase.com/merseyside/index.php)

[https://www.paranormaldatabase.com/hotspots/Darlington.phpNort East and Yorkshire](https://www.paranormaldatabase.com/tyneandwear/index.php)

[Northumberland](https://www.paranormaldatabase.com/northumberland/nhumdata.php)

[La ville de Darlington](https://www.paranormaldatabase.com/hotspots/Darlington.php)

Comme on le voit, la ville de Darlington commence par un D majuscule tandis que pour le lien menant vers la ville d'Aberdeen, le nom de la ville commence par un a minuscule.

## 2.4 MLD

![mld](mld.png)

(![debut](Debut arbre.png))

![alt text](Fin arbre.png)

## 2.5 Base de données

Nous avons construit la base de données (fichier init.sql) puis nous avons transmis des données à travers le fichier migration.sql qui permet de vérifier si les données ont été ajoutées.

Pour ajouter une région:

```
INSERT INTO elements (nom, parent, genre) VALUES ('South East England', NULL, 'regions');
```

Pour ajouter un report:

```
INSERT INTO elements (nom, parent, genre) VALUES ('Recent Additions', NULL, 'reports');
```

```
 select * from elements;
```

```
| id | nom                      | parent | genre   |
+----+--------------------------+--------+---------+
|  1 | South East England       |   NULL | regions |
|  2 | Greater London           |   NULL | regions |
|  3 | East of England          |   NULL | regions |
|  4 | East Midlands            |   NULL | regions |
|  5 | West Midlands            |   NULL | regions |
|  6 | Northern Ireland         |   NULL | regions |
|  7 | Other Regions            |   NULL | regions |
|  8 | Republic of Ireland      |   NULL | regions |
|  9 | Wales                    |   NULL | regions |
| 10 | Scotland                 |   NULL | regions |
| 11 | North West England       |   NULL | regions |
| 12 | North East and Yorkshire |   NULL | regions |
| 13 | Recent Additions         |   NULL | reports |
| 14 | Reports:Places           |   NULL | reports |
| 15 | Contribute               |   NULL | reports |
| 16 | Calendar                 |   NULL | reports |
+----+--------------------------+--------+---------+
```

## 2.6 Données scrappées.

```plaintext
/ssl/contact001.html
/index.html#england
/regions/scotland.html
/regions/wales.html
/regions/northernireland.html
/regions/ireland.html
/reports/reports-type.html
/reports/reports.htm
/reports/reports-people.html
/calendar/Pages/calendar.html
/recent/index.php
/index.html
/berkshire/berkdata.php
/buckinghamshire/buckdata.php
/hampshire/hampdata.php
/islewight/wighdata.php
/kent/kentdata.php
/oxfordshire/oxfodata.php
/surrey/surrdata.php
/sussex/suspages/sussdata.php
/hotspots/brighton.php
/hotspots/canterbury.php
/hotspots/eastbourne.php
/hotspots/pluckley.php
/hotspots/portsmouth.php
/hotspots/rye.php
/hotspots/winchester.php
#myHeader```
```

## 2.7 Les données ajoutées en SQL

```plaintext
select * from cas;
```

```plaintext
|  1 | A strange-looking coach with horses sinking into water | Antrim - Antrim Castle | Haunting Manifestation | 1870-05-31 | A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building | NULL  |    NULL |
```

## 2.8 Données du fichier

## 2.8.1 Northern Ireland et la province de Antrim

Page scrapée:

[Northern Ireland](https://www.paranormaldatabase.com/ireland/antrim.php)

```
Titre: Sinking Coach
Location: Type:
Date / Time: Further Comments:
Further Comments: None
<p><img class="w3-card" src="../../thumbs/Antrim-Antrim-Castle.jpg" title="A strange-looking coach with horses sinking into water."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> A strange-looking coach with horses sinking into water.</small></span> <p><h4><span class="w3-border-bottom">Sinking Coach</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Antrim Castle<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 31 May (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">Sinking Coach</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Antrim Castle<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 31 May (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Clotworthy House<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1993<br/>
<span class="w3-border-bottom">Further Comments:</span> Investigating strange sounds after dark, a security guard spotted a white figure standing in a room. The guard closed the only door leading to the room to trap the figure, but the apparition vanished without trace.</p></p>
<p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Clotworthy House<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1993<br/>
<span class="w3-border-bottom">Further Comments:</span> Investigating strange sounds after dark, a security guard spotted a white figure standing in a room. The guard closed the only door leading to the room to trap the figure, but the apparition vanished without trace.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Bloody Puddle</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Mussenden Temple library<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A mysterious patch of blood occasionally appears on the floor of this building, evaporating away a few minutes later.</p></p>
<p><h4><span class="w3-border-bottom">Bloody Puddle</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim - Mussenden Temple library<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A mysterious patch of blood occasionally appears on the floor of this building, evaporating away a few minutes later.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Hanged People</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim Town - Corner of the Belmont Road and Cunningham Way<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A few residents to the area have spotted ghosts of people hanging from the trees. Local folklore says that this was once the location of a hanging tree.</p></p>
<p><h4><span class="w3-border-bottom">Hanged People</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim Town - Corner of the Belmont Road and Cunningham Way<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A few residents to the area have spotted ghosts of people hanging from the trees. Local folklore says that this was once the location of a hanging tree.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Female Cyclist</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim town - Dublin Road (poss. British Road), towards the airport<br/>
<span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br/>
<span class="w3-border-bottom">Date / Time:</span> 1991<br/>
<span class="w3-border-bottom">Further Comments:</span> While travelling to work, a man spotted a young girl wearing a long 1900's type dress cross the road on an old fashioned bicycle. When the witness reached the point where he had seen her, he realised that there were no gaps in the heavy hedge rows and that on either side of the road deep ditches leading into heavily ploughed fields made it impossible for anyone to cross at that point.</p></p>
<p><h4><span class="w3-border-bottom">Female Cyclist</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Antrim town - Dublin Road (poss. British Road), towards the airport<br/>
<span class="w3-border-bottom">Type:</span> Unknown Ghost Type<br/>
<span class="w3-border-bottom">Date / Time:</span> 1991<br/>
<span class="w3-border-bottom">Further Comments:</span> While travelling to work, a man spotted a young girl wearing a long 1900's type dress cross the road on an old fashioned bicycle. When the witness reached the point where he had seen her, he realised that there were no gaps in the heavy hedge rows and that on either side of the road deep ditches leading into heavily ploughed fields made it impossible for anyone to cross at that point.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Operation Big Cat</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballybogy (aka Ballybogey) - General area and surrounding countryside<br/>
<span class="w3-border-bottom">Type:</span> ABC<br/>
<span class="w3-border-bottom">Date / Time:</span> 2003<br/>
<span class="w3-border-bottom">Further Comments:</span> A joint army and police team created 'Operation Big Cat' after two dead calves were found and a local man admitted releasing a puma and a panther from his private collection. Although there were dozens of sightings and livestock attacks, the cats were never caught.</p></p>
<p><h4><span class="w3-border-bottom">Operation Big Cat</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballybogy (aka Ballybogey) - General area and surrounding countryside<br/>
<span class="w3-border-bottom">Type:</span> ABC<br/>
<span class="w3-border-bottom">Date / Time:</span> 2003<br/>
<span class="w3-border-bottom">Further Comments:</span> A joint army and police team created 'Operation Big Cat' after two dead calves were found and a local man admitted releasing a puma and a panther from his private collection. Although there were dozens of sightings and livestock attacks, the cats were never caught.</p>
<br/>
<br/>
<br/>
<p><img class="w3-card" src="../../thumbs/Ballycastle-Graveyard.jpg" title="A ghostly black nun."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> A ghostly black nun.</small></span> <p><h4><span class="w3-border-bottom">Black Nun</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballycastle - Graveyard<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A local legend says if you walk around Julia McQuillan's (aka the black nun) grave seven times clockwise, seven times anticlockwise and then place your hand through the hole in the grave, you will summon the ghost of the nun. McQuillan was a recluse who became renown after issuing seven prophesies.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">Black Nun</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballycastle - Graveyard<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A local legend says if you walk around Julia McQuillan's (aka the black nun) grave seven times clockwise, seven times anticlockwise and then place your hand through the hole in the grave, you will summon the ghost of the nun. McQuillan was a recluse who became renown after issuing seven prophesies.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Lady Isobel Shaw</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballygally - Ballygally Castle Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Twentieth century<br/>
<span class="w3-border-bottom">Further Comments:</span> Lady Isobel jumped to her death from an upper window to escape a slow lingering death administrated by her husband. She now knocks on various doors before running away. Madame Nixon is named as another ghost to be found here and can be recognised by the rustling of her dress.</p></p>
<p><h4><span class="w3-border-bottom">Lady Isobel Shaw</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballygally - Ballygally Castle Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Twentieth century<br/>
<span class="w3-border-bottom">Further Comments:</span> Lady Isobel jumped to her death from an upper window to escape a slow lingering death administrated by her husband. She now knocks on various doors before running away. Madame Nixon is named as another ghost to be found here and can be recognised by the rustling of her dress.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Hell Gate</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br/>
<span class="w3-border-bottom">Type:</span> Other<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Dundermot Mound is reported to be the location of a secret entrance to hell - occasionally it opens, releasing demons who take any witnesses straight to the underworld.</p></p>
<p><h4><span class="w3-border-bottom">Hell Gate</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br/>
<span class="w3-border-bottom">Type:</span> Other<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Dundermot Mound is reported to be the location of a secret entrance to hell - occasionally it opens, releasing demons who take any witnesses straight to the underworld.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A phantom coach and four rides around the area. The ghostly driver will ask anyone he spots 'is the bridge at Glarryford still up?' - any answer the witness gives will ensure that they will die within a year (so I suggest looking at your feet and saying nothing).</p></p>
<p><h4><span class="w3-border-bottom">Coachman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Dundermot Mound<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A phantom coach and four rides around the area. The ghostly driver will ask anyone he spots 'is the bridge at Glarryford still up?' - any answer the witness gives will ensure that they will die within a year (so I suggest looking at your feet and saying nothing).</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Road leading to the White Gates, Crebilly Road area<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> A robber escaping from a house rode into a thin piece of wire tightly pulled between the gateposts here, losing his head in the process. His decapitated shade is either seen or heard at Halloween, still on horseback.</p></p>
<p><h4><span class="w3-border-bottom">Headless Horseman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Road leading to the White Gates, Crebilly Road area<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 31 October (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> A robber escaping from a house rode into a thin piece of wire tightly pulled between the gateposts here, losing his head in the process. His decapitated shade is either seen or heard at Halloween, still on horseback.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Carrie</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Tullyglass Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2000s<br/>
<span class="w3-border-bottom">Further Comments:</span> Carrie is believed to have died in the tower around two hundred years ago when the Tullyglass was a mansion house.</p></p>
<p><h4><span class="w3-border-bottom">Carrie</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymena - Tullyglass Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2000s<br/>
<span class="w3-border-bottom">Further Comments:</span> Carrie is believed to have died in the tower around two hundred years ago when the Tullyglass was a mansion house.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">George Hutchinsons</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoney - Main Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Midnight, every Friday the thirteenth (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> With a large metal ball chained to his ankle, George 'Bloody' Hutchinsons walks one way down the street before turning back again. It is reported that anyone who can prevent him from completing his journey will dismiss the spirit forever.</p></p>
<p><h4><span class="w3-border-bottom">George Hutchinsons</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoney - Main Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Midnight, every Friday the thirteenth (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> With a large metal ball chained to his ankle, George 'Bloody' Hutchinsons walks one way down the street before turning back again. It is reported that anyone who can prevent him from completing his journey will dismiss the spirit forever.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Pipe Smoker</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoney - Private residence<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2010s<br/>
<span class="w3-border-bottom">Further Comments:</span> A few ghosts are said to haunt this property. A couple of female forms flitter around, while a male pipe smoker and an authoritative man are also reported. Poltergeist-like activities have included banging sounds, doors opening, and footsteps.</p></p>
<p><h4><span class="w3-border-bottom">Pipe Smoker</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoney - Private residence<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2010s<br/>
<span class="w3-border-bottom">Further Comments:</span> A few ghosts are said to haunt this property. A couple of female forms flitter around, while a male pipe smoker and an authoritative man are also reported. Poltergeist-like activities have included banging sounds, doors opening, and footsteps.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Cleaners</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoy - Ballymoy Castle<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Saturday (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Invisible wraiths are said to sweep the castle clean each Saturday.</p></p>
<p><h4><span class="w3-border-bottom">Cleaners</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Ballymoy - Ballymoy Castle<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Saturday (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Invisible wraiths are said to sweep the castle clean each Saturday.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">John Savage</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - The Brickyard<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Nineteenth century landlord John Savage slit his own throat after suffering what is thought to be a state of depression. His ghost was said to haunt the brickyard.</p></p>
<p><h4><span class="w3-border-bottom">John Savage</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - The Brickyard<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Nineteenth century landlord John Savage slit his own throat after suffering what is thought to be a state of depression. His ghost was said to haunt the brickyard.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Helena Blunden</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - The Irish Linen Mill (currently a print shop)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> N/A<br/>
<span class="w3-border-bottom">Further Comments:</span> A known hoax which claimed a woman named Helena fell on the basement stairs and was dead before she hit the bottom step.</p></p>
<p><h4><span class="w3-border-bottom">Helena Blunden</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - The Irish Linen Mill (currently a print shop)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> N/A<br/>
<span class="w3-border-bottom">Further Comments:</span> A known hoax which claimed a woman named Helena fell on the basement stairs and was dead before she hit the bottom step.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Figure in Carriage</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Ulster Folk and Transport Museum<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> October 2017<br/>
<span class="w3-border-bottom">Further Comments:</span> Local press published a photograph of a spooky looking figure sitting in a train carriage, although the 'ghost' is most likely accidently created using a slow shutter speed.</p></p>
<p><h4><span class="w3-border-bottom">Figure in Carriage</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Ulster Folk and Transport Museum<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> October 2017<br/>
<span class="w3-border-bottom">Further Comments:</span> Local press published a photograph of a spooky looking figure sitting in a train carriage, although the 'ghost' is most likely accidently created using a slow shutter speed.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Woman Drinking Coffee</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Vicarage Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This female ghost walks down the street carrying a cup of coffee in one hand and a cigarette in her other.</p></p>
<p><h4><span class="w3-border-bottom">Woman Drinking Coffee</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Vicarage Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This female ghost walks down the street carrying a cup of coffee in one hand and a cigarette in her other.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">John Herdman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Waterworks<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Shot in 1862, John Herdman is said to haunt the area of the Waterworks.</p></p>
<p><h4><span class="w3-border-bottom">John Herdman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Waterworks<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> Shot in 1862, John Herdman is said to haunt the area of the Waterworks.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Shadowy Figures</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - York Road Railway Station<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1970s - present<br/>
<span class="w3-border-bottom">Further Comments:</span> A strange person has been reported sitting in the locked canteen at night. This may be the same figure glimpsed in the running sheds - during a bungled robbery, a man was either shot or beaten and subsequently died from his injuries. Night staff have also reported hearing disembodied footsteps in the area and on one occasion, having a fire extinguisher thrown at them.</p></p>
<p><h4><span class="w3-border-bottom">Shadowy Figures</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - York Road Railway Station<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1970s - present<br/>
<span class="w3-border-bottom">Further Comments:</span> A strange person has been reported sitting in the locked canteen at night. This may be the same figure glimpsed in the running sheds - during a bungled robbery, a man was either shot or beaten and subsequently died from his injuries. Night staff have also reported hearing disembodied footsteps in the area and on one occasion, having a fire extinguisher thrown at them.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - 91 Beechmount Grove (no longer standing)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> June 1989<br/>
<span class="w3-border-bottom">Further Comments:</span> John Skillen released a book of his experiences in this house where he lived with his family. During their time here, the ghost tossed John against a wall and slapped his children. The family moved away after the attacks escalated.</p></p>
<p><h4><span class="w3-border-bottom">Victorian Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - 91 Beechmount Grove (no longer standing)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> June 1989<br/>
<span class="w3-border-bottom">Further Comments:</span> John Skillen released a book of his experiences in this house where he lived with his family. During their time here, the ghost tossed John against a wall and slapped his children. The family moved away after the attacks escalated.</p>
<br/>
<br/>
<br/>
<p><img class="w3-card" src="../../thumbs/Belfast-Above-Yorkgate.jpg" title="A rather dramatic green UFO over Belfast."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> A rather dramatic green UFO over Belfast.</small></span> <p><h4><span class="w3-border-bottom">Green Triangle</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Above Yorkgate shopping centre<br/>
<span class="w3-border-bottom">Type:</span> UFO<br/>
<span class="w3-border-bottom">Date / Time:</span> 12 November 2006<br/>
<span class="w3-border-bottom">Further Comments:</span> Shoppers at the centre reported seeing a green triangular ufo cross the sky above them.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">Green Triangle</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Above Yorkgate shopping centre<br/>
<span class="w3-border-bottom">Type:</span> UFO<br/>
<span class="w3-border-bottom">Date / Time:</span> 12 November 2006<br/>
<span class="w3-border-bottom">Further Comments:</span> Shoppers at the centre reported seeing a green triangular ufo cross the sky above them.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Galloper Thompson</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Alexandra Park Avenue<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br/>
<span class="w3-border-bottom">Further Comments:</span> A member of the Thompson family who loved horses died and took to galloping along the Avenue after dark (the horse taken from the family stable and normally found exhausted the following day). The haunting ceased after the ghost, nicknamed 'Galloper Thompson', was caught in a bottle and tossed into the sea.</p></p>
<p><h4><span class="w3-border-bottom">Galloper Thompson</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Alexandra Park Avenue<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Nineteenth century<br/>
<span class="w3-border-bottom">Further Comments:</span> A member of the Thompson family who loved horses died and took to galloping along the Avenue after dark (the horse taken from the family stable and normally found exhausted the following day). The haunting ceased after the ghost, nicknamed 'Galloper Thompson', was caught in a bottle and tossed into the sea.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Black Man</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Arden Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> October 1972<br/>
<span class="w3-border-bottom">Further Comments:</span> A brief myth which sprang up around Halloween, the streets were said to be the haunting ground of a black figure which could change size.</p></p>
<p><h4><span class="w3-border-bottom">Black Man</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Belfast - Arden Street<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> October 1972<br/>
<span class="w3-border-bottom">Further Comments:</span> A brief myth which sprang up around Halloween, the streets were said to be the haunting ground of a black figure which could change size.</p>
<br/>
<br/>
<br/>
<a class="w3-button h4" href="?pageNum_paradata=1&totalRows_paradata=72">next</a>
<a class="w3-button h4" href="?pageNum_paradata=2&totalRows_paradata=72">last</a>
<p>
<button class="w3-button w3-block h4 w3-border"><a href="/regions/northernireland.html">Return to Northern Ireland</a></button></p>
<a href="/regions/northernireland.html">Return to Northern Ireland</a>
<p></p>
<a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img alt="Contribute to the Paranormal Database" src="/images/front/cont2.jpg" style="width:100%" title="Contribute to the Paranormal Database"/>
</div></a>
<p></p>
<a href="/regions/northernireland.html"> <div class="w3-hover-opacity w3-image"> <img alt="View Northern Ireland records" src="/images/front/ni.jpg" style="width:100%" title="View Northern Ireland records"/>
</div></a>
<p></p>
<a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img alt="View the Paranormal Database Calendar" src="/images/front/calendar.jpg" style="width:100%" title="View the Paranormal Database Calendar"/>
</div></a>
<p></p>
<a href="/regions/ireland.html"> <div class="w3-hover-opacity w3-image"> <img alt="View Republic of Ireland records" src="/images/front/roi.jpg" style="width:100%" title="View Republic of Ireland records"/>
</div></a>
<p></p>
```

La partie du travail qui permet d'ajouter des données en SQL:

## 2.8.2 Les données en SQL de Northern Ireland

Lien pour scraper: [Northern Ireland](https://www.paranormaldatabase.com/ireland/antrim.php)

```
select * from cas;
```

```
| id | title                                                  | locations                                                             | elem_note              | date       | comments                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | month | weather |
+----+--------------------------------------------------------+-----------------------------------------------------------------------+------------------------+------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------+---------+
|  1 | A strange-looking coach with horses sinking into water | Antrim - Antrim Castle                                                | Haunting Manifestation | 1870-05-31 | A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building | NULL  |    NULL |

```

## 2.8.3 Les données du Pays de Galles (Wales)

La page scrapée:

```
<p><img class="w3-card" src="../../thumbs/2205womaninshroud.jpg" title="Shrouded Woman, public domain."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> Shrouded Woman, public domain.</small></span> <p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Aberhafesp - Aberhafesp Hall<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The phantom white woman is said to make her way from the nearby church to a door on the east side of the hall.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">White Lady</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Aberhafesp - Aberhafesp Hall<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The phantom white woman is said to make her way from the nearby church to a door on the east side of the hall.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">David Bowen</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Aberhowy - Road to village from Cardigan Bay<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights (last reported 1956?)<br/>
<span class="w3-border-bottom">Further Comments:</span> The ghost of David on his push bike is said to guide people along the road when the weather is densely foggy and dangerous. Bowen lost his son in an automobile accident on such a night, and now tries to ensure such a tragedy never happens again.</p></p>
<p><h4><span class="w3-border-bottom">David Bowen</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Aberhowy - Road to village from Cardigan Bay<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Weather Dependent: Foggy nights (last reported 1956?)<br/>
<span class="w3-border-bottom">Further Comments:</span> The ghost of David on his push bike is said to guide people along the road when the weather is densely foggy and dangerous. Bowen lost his son in an automobile accident on such a night, and now tries to ensure such a tragedy never happens again.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Abermule - Gate along the railway track to Montgomery<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Winter 1921<br/>
<span class="w3-border-bottom">Further Comments:</span> A woman in black spoke to a teenager along this track, expressing her disappointment that another train was not due for two hours. The witness believed that the woman was a victim of the rail crash here which took seventeen lives on 26 January 1921.</p></p>
<p><h4><span class="w3-border-bottom">Woman in Black</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Abermule - Gate along the railway track to Montgomery<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Winter 1921<br/>
<span class="w3-border-bottom">Further Comments:</span> A woman in black spoke to a teenager along this track, expressing her disappointment that another train was not due for two hours. The witness believed that the woman was a victim of the rail crash here which took seventeen lives on 26 January 1921.</p>
<br/>
<br/>
<br/>
<p><img class="w3-card" src="../../thumbs/Berriew-Field-between-Hafodafel.jpg" title="A Welsh fairy queen sighting."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> A Welsh fairy queen sighting.</small></span> <p><h4><span class="w3-border-bottom">Gathering</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Berriew - Field between Hafodafel  and Pen-y-Llwyn<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A child walking this path with his aunt watched many people moving around a sheep pen, including a pale woman wearing a red jacket and a crown. When the child mentioned it to his aunt a little later, she said he must have been dreaming, as only ruins stood in that field.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">Gathering</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Berriew - Field between Hafodafel  and Pen-y-Llwyn<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A child walking this path with his aunt watched many people moving around a sheep pen, including a pale woman wearing a red jacket and a crown. When the child mentioned it to his aunt a little later, she said he must have been dreaming, as only ruins stood in that field.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Gwyllion</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Black Mountain - Likely area north of Crickhowell<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A Gwyllion which took the form of an old lady was said to haunt this mountain. She would be seen just in front of walkers and appear to slowly hobble, although no matter how fast witnesses would travel, she would always stay ahead. The entity would try to lure witnesses into marshland.</p></p>
<p><h4><span class="w3-border-bottom">Gwyllion</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Black Mountain - Likely area north of Crickhowell<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A Gwyllion which took the form of an old lady was said to haunt this mountain. She would be seen just in front of walkers and appear to slowly hobble, although no matter how fast witnesses would travel, she would always stay ahead. The entity would try to lure witnesses into marshland.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Once Upon a Time</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Abercamlais House<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The main bedroom was once said to be haunted although the finer points of the phantom are not known.</p></p>
<p><h4><span class="w3-border-bottom">Once Upon a Time</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Abercamlais House<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The main bedroom was once said to be haunted although the finer points of the phantom are not known.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Caer Bannau<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This woman in white was said to have been a princess - anyone who carried the ghost to a nearby churchyard could put her soul to rest.</p></p>
<p><h4><span class="w3-border-bottom">Woman in White</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Caer Bannau<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This woman in white was said to have been a princess - anyone who carried the ghost to a nearby churchyard could put her soul to rest.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Glowing Light</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Chapel close to Neuadd<br/>
<span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A glowing light was seen after dark by a handful of separate witnesses. Soon after, a farmer ploughing a field close to where the light had been seen unearthed a body buried in a trunk. After the remains were removed, the light was seen no more.</p></p>
<p><h4><span class="w3-border-bottom">Glowing Light</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Chapel close to Neuadd<br/>
<span class="w3-border-bottom">Type:</span> Post-Mortem Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A glowing light was seen after dark by a handful of separate witnesses. Soon after, a farmer ploughing a field close to where the light had been seen unearthed a body buried in a trunk. After the remains were removed, the light was seen no more.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Two Men</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Guildhall<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A popular place for event-based ghost hunts, the site is said to be haunted by a young girl, and two men who sit in the upper gallery.</p></p>
<p><h4><span class="w3-border-bottom">Two Men</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - Guildhall<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A popular place for event-based ghost hunts, the site is said to be haunted by a young girl, and two men who sit in the upper gallery.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Grey Horse</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - River Honddu<br/>
<span class="w3-border-bottom">Type:</span> Cryptozoology<br/>
<span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br/>
<span class="w3-border-bottom">Further Comments:</span> Home of a Welsh water horse, this creature who tempt people to sit on it before terrifying the rider by dragging them over rough land and through the air.</p></p>
<p><h4><span class="w3-border-bottom">Grey Horse</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - River Honddu<br/>
<span class="w3-border-bottom">Type:</span> Cryptozoology<br/>
<span class="w3-border-bottom">Date / Time:</span> Nineteenth century?<br/>
<span class="w3-border-bottom">Further Comments:</span> Home of a Welsh water horse, this creature who tempt people to sit on it before terrifying the rider by dragging them over rough land and through the air.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Gliding Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - The Welsh Bookshop, The Struet<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This entity dressed in eighteenth century clothing vanishes through a wall and emerges in the building next door.</p></p>
<p><h4><span class="w3-border-bottom">Gliding Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon - The Welsh Bookshop, The Struet<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This entity dressed in eighteenth century clothing vanishes through a wall and emerges in the building next door.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Fairy Mine</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon Beacons - Exact area unknown, but possibly mountain closest to Aberbeeg<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A man crossing the mountain watched a fairy coal mine at work. The operation was taking place in complete silence, and the witness knew that no mortal mine existed in the area.</p></p>
<p><h4><span class="w3-border-bottom">Fairy Mine</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon Beacons - Exact area unknown, but possibly mountain closest to Aberbeeg<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> A man crossing the mountain watched a fairy coal mine at work. The operation was taking place in complete silence, and the witness knew that no mortal mine existed in the area.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Secret Door</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon Beacons - Llyn Cwm Llwch (somewhere around the area)<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Once a year, a secret door opens here which will take mortals to the fairy realm. The door was once always open, but after a man stole a fairy flower, access was restricted. The area is also home to an old woman who uses music to lure the weak willed into the lake waters where they drown - once she has killed nine hundred people, she will regain her youth and become mortal.</p></p>
<p><h4><span class="w3-border-bottom">Secret Door</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Brecon Beacons - Llyn Cwm Llwch (somewhere around the area)<br/>
<span class="w3-border-bottom">Type:</span> Fairy<br/>
<span class="w3-border-bottom">Date / Time:</span> 01 May (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Once a year, a secret door opens here which will take mortals to the fairy realm. The door was once always open, but after a man stole a fairy flower, access was restricted. The area is also home to an old woman who uses music to lure the weak willed into the lake waters where they drown - once she has killed nine hundred people, she will regain her youth and become mortal.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Darting White Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Bronllys - All over the village (including Pontywal)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Circa 1908<br/>
<span class="w3-border-bottom">Further Comments:</span> This white entity was seen several times silently moving at speed around the village, sometimes disappearing only to reappear moments later draped in black. Even though occasionally pursued by locals, it could never be caught, and even once appeared in Pontywal (the home of Breconshire's chief constable).</p></p>
<p><h4><span class="w3-border-bottom">Darting White Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Bronllys - All over the village (including Pontywal)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Circa 1908<br/>
<span class="w3-border-bottom">Further Comments:</span> This white entity was seen several times silently moving at speed around the village, sometimes disappearing only to reappear moments later draped in black. Even though occasionally pursued by locals, it could never be caught, and even once appeared in Pontywal (the home of Breconshire's chief constable).</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Friendly Thing</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Builth Wells - Llanelwedd Arms Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The ghost which was said to haunt this site was described as friendly.</p></p>
<p><h4><span class="w3-border-bottom">Friendly Thing</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Builth Wells - Llanelwedd Arms Hotel<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> The ghost which was said to haunt this site was described as friendly.</p>
<br/>
<br/>
<br/>
<p><img class="w3-card" src="../../thumbs/Builth-Wells-stones.jpg" title="The mythical boat Twrch Trwyth."/><br/><span class="w3-text-grey"><i aria-hidden="true" class="fa fa-camera-retro"></i> <small> The mythical boat Twrch Trwyth.</small></span> <p><h4><span class="w3-border-bottom">Carn Cabal</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Builth Wells - Pile of stones (no longer present)<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> King Arthur's dog Cavall left his footprint upon a stone while hunting the mythical boar Twrch Trwyth (Troyt). Arthur had the footprint placed on top of a cairn, and if removed, the marked stone would always return the following day.</p></p>
<br/>
<p><h4><span class="w3-border-bottom">Carn Cabal</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Builth Wells - Pile of stones (no longer present)<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> King Arthur's dog Cavall left his footprint upon a stone while hunting the mythical boar Twrch Trwyth (Troyt). Arthur had the footprint placed on top of a cairn, and if removed, the marked stone would always return the following day.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Walled Up Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Burgedin - Lock-keepers cottage, Montgomery Canal<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This building is said to be home to a woman bricked up in the basement after trying to run off with a man.</p></p>
<p><h4><span class="w3-border-bottom">Walled Up Woman</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Burgedin - Lock-keepers cottage, Montgomery Canal<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Unknown<br/>
<span class="w3-border-bottom">Further Comments:</span> This building is said to be home to a woman bricked up in the basement after trying to run off with a man.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Killer Cat</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Clyro - General area<br/>
<span class="w3-border-bottom">Type:</span> ABC<br/>
<span class="w3-border-bottom">Date / Time:</span> Summer 1989<br/>
<span class="w3-border-bottom">Further Comments:</span> This mysterious creature killed dozens of sheep over the summer months, tearing out their throats. Two people who saw the beast said it was dark in colour and larger than a dog. Not all agreed with the belief that the creature was a cat, some stating that the entity was a gwyllgi, a large black dog.</p></p>
<p><h4><span class="w3-border-bottom">Killer Cat</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Clyro - General area<br/>
<span class="w3-border-bottom">Type:</span> ABC<br/>
<span class="w3-border-bottom">Date / Time:</span> Summer 1989<br/>
<span class="w3-border-bottom">Further Comments:</span> This mysterious creature killed dozens of sheep over the summer months, tearing out their throats. Two people who saw the beast said it was dark in colour and larger than a dog. Not all agreed with the belief that the creature was a cat, some stating that the entity was a gwyllgi, a large black dog.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Fish Stone</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Crickhowell - Stone by the River Usk<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Once a year this strangely shaped stone dives into the local river and goes for a swim.</p></p>
<p><h4><span class="w3-border-bottom">Fish Stone</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Crickhowell - Stone by the River Usk<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> 23 June (or Midsummer Eve) (reoccurring)<br/>
<span class="w3-border-bottom">Further Comments:</span> Once a year this strangely shaped stone dives into the local river and goes for a swim.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Child</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Cyfronydd - Private residence<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2000s<br/>
<span class="w3-border-bottom">Further Comments:</span> A ghostly child was said to haunt this property, giggling and knocking on doors. Items would also be hidden around the building.</p></p>
<p><h4><span class="w3-border-bottom">Child</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Cyfronydd - Private residence<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 2000s<br/>
<span class="w3-border-bottom">Further Comments:</span> A ghostly child was said to haunt this property, giggling and knocking on doors. Items would also be hidden around the building.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Glowing Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Dylife - Old mine tunnels<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1980s<br/>
<span class="w3-border-bottom">Further Comments:</span> A man exploring the tunnels watched a blue glowing light, the size of a small person, moving around ahead of him. Old miners once told tales of balls of light which moved around the tunnels until they reached the surface, where they floated off into the sky.</p></p>
<p><h4><span class="w3-border-bottom">Glowing Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Dylife - Old mine tunnels<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1980s<br/>
<span class="w3-border-bottom">Further Comments:</span> A man exploring the tunnels watched a blue glowing light, the size of a small person, moving around ahead of him. Old miners once told tales of balls of light which moved around the tunnels until they reached the surface, where they floated off into the sky.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Thirsty Rocks</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Evenjobb - Four stones near Hindwell Pool<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Midnight<br/>
<span class="w3-border-bottom">Further Comments:</span> Each night on the stroke of twelve, these four standing stones walk to Hindwell Pool to quench their thirst before returning before the sun rises.</p></p>
<p><h4><span class="w3-border-bottom">Thirsty Rocks</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Evenjobb - Four stones near Hindwell Pool<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Midnight<br/>
<span class="w3-border-bottom">Further Comments:</span> Each night on the stroke of twelve, these four standing stones walk to Hindwell Pool to quench their thirst before returning before the sun rises.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Hooded Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Glyn Tarell - Heol Fanog Farm (aka Witch Farm, aka Hellfire Farm)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1989 - 1996<br/>
<span class="w3-border-bottom">Further Comments:</span> A high number of strange events were reputed to have occurred in this farm, including the disappearance or premature death of pets, a ghostly old woman who appeared to children, a hooded entity, and the stifling feeling of oppression and negativity.</p></p>
<p><h4><span class="w3-border-bottom">Hooded Figure</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Glyn Tarell - Heol Fanog Farm (aka Witch Farm, aka Hellfire Farm)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> 1989 - 1996<br/>
<span class="w3-border-bottom">Further Comments:</span> A high number of strange events were reputed to have occurred in this farm, including the disappearance or premature death of pets, a ghostly old woman who appeared to children, a hooded entity, and the stifling feeling of oppression and negativity.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Dennis</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Hay-on-Wye - Baskerville Hall Hotel (aka Clyro Court)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Dennis circa October 2005, hound in 1998<br/>
<span class="w3-border-bottom">Further Comments:</span> Amanda Hyde, writing for the Times Online, documented her less than convincing experiences during a ghost hunt on the site, during which she encountered Dennis (via a medium) and communicated with Edward using a glass. More convincing is the encounter a chef reported in 1998. He reported seeing a shadow the size and shape of a large dog in the kitchen a few minutes before feeling a nip on his right thigh. He cried out, three nearby staff asking what was wrong, although they were disbelieving when he told them what he had seen and felt. The following day a heavily bruised bite mark appeared on his leg.</p></p>
<p><h4><span class="w3-border-bottom">Dennis</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Hay-on-Wye - Baskerville Hall Hotel (aka Clyro Court)<br/>
<span class="w3-border-bottom">Type:</span> Haunting Manifestation<br/>
<span class="w3-border-bottom">Date / Time:</span> Dennis circa October 2005, hound in 1998<br/>
<span class="w3-border-bottom">Further Comments:</span> Amanda Hyde, writing for the Times Online, documented her less than convincing experiences during a ghost hunt on the site, during which she encountered Dennis (via a medium) and communicated with Edward using a glass. More convincing is the encounter a chef reported in 1998. He reported seeing a shadow the size and shape of a large dog in the kitchen a few minutes before feeling a nip on his right thigh. He cried out, three nearby staff asking what was wrong, although they were disbelieving when he told them what he had seen and felt. The following day a heavily bruised bite mark appeared on his leg.</p>
<br/>
<br/>
<br/>
<p> <p><h4><span class="w3-border-bottom">Overnight Construction</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Hay-on-Wye - Hay Castle<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Twelfth century<br/>
<span class="w3-border-bottom">Further Comments:</span> The giant Moll Walbee, known also as Maud de Breos, built this castle in a single night!</p></p>
<p><h4><span class="w3-border-bottom">Overnight Construction</span></h4></p>
<p><span class="w3-border-bottom">Location:</span> Hay-on-Wye - Hay Castle<br/>
<span class="w3-border-bottom">Type:</span> Legend<br/>
<span class="w3-border-bottom">Date / Time:</span> Twelfth century<br/>
<span class="w3-border-bottom">Further Comments:</span> The giant Moll Walbee, known also as Maud de Breos, built this castle in a single night!</p>
<br/>
<br/>
<br/>
<a class="w3-button h4" href="?pageNum_paradata=1&totalRows_paradata=68">next</a>
<a class="w3-button h4" href="?pageNum_paradata=2&totalRows_paradata=68">last</a>
<p>
<button class="w3-button w3-block h4 w3-border"><a href="/regions/wales.html">Return to Welsh Preserved Counties</a></button></p>
<a href="/regions/wales.html">Return to Welsh Preserved Counties</a>
<p></p>
<a href="/ssl/contact001.html"> <div class="w3-hover-opacity w3-image"> <img alt="Contribute to the Paranormal Database" src="/images/front/cont2.jpg" style="width:100%" title="Contribute to the Paranormal Database"/>
</div></a>
<p></p>
<a href="/regions/scotland.html"> <div class="w3-hover-opacity w3-image"> <img alt="View Scotland records" src="/images/secondlevel/Lothian.jpg" style="width:100%" title="View Scotland records"/>
</div></a>
<p></p>
<a href="/calendar/Pages/calendar.html"> <div class="w3-hover-opacity w3-image"> <img alt="View the Paranormal Database Calendar" src="/images/front/calendar.jpg" style="width:100%" title="View the Paranormal Database Calendar"/>
</div></a>
<p></p>
<a href="/regions/eastengland.html"> <div class="w3-hover-opacity w3-image"> <img alt="View East of England records" src="/images/front/eastengland.jpg" style="width:100%" title="View East of England records"/>
</div></a>
<p></p>
```

[Wales](https://www.paranormaldatabase.com/wales/Powys.php)

```
 select * from cas;

```

```
| 46 | Child                                                  | Cyfronydd - Private residence                                                  | Haunting Manifestation    | 2000-01-01 | A ghostly child was said to haunt this property, giggling and knocking on doors. Items would also be hidden around the building.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | NULL  |    NULL |
| 47 | Glowing Figure                                         | Dylife - Old mine tunnels                                                      | Haunting Manifestation    | 1980-01-01 | A man exploring the tunnels watched a blue glowing light, the size of a small person, moving around ahead of him. Old miners once told tales of balls of light which moved around the tunnels until they reached the surface, where they floated off into the sky.                                                                                                                                                                                                                                                                                                                                                               | NULL  |    NULL |
| 48 | Thirsty Rocks                                          | Evenjobb - Four stones near Hindwell Pool                                      | Legend                    | 1880-01-01 | Each night on the stroke of twelve, these four standing stones walk to Hindwell Pool to quench their thirst before returning before the sun rises.                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | NULL  |    NULL |
| 49 | Hooded Figure                                          | Glyn Tarell - Heol Fanog Farm (aka Witch Farm, aka Hellfire Farm)              | Haunting Manifestation    | 1989-01-01 | A high number of strange events were reputed to have occurred in this farm, including the disappearance or premature death of pets, a ghostly old woman who appeared to children, a hooded entity, and the stifling feeling of oppression and negativity.                                                                                                                                                                                                                                                                                                                                                                        | NULL  |    NULL |
| 50 | Dennis                                                 | Hay-on-Wye - Baskerville Hall Hotel (aka Clyro Court)                          | Haunting Manifestation    | 2005-10-01 | Amanda Hyde, writing for the Times Online, documented her less than convincing experiences during a ghost hunt on the site, during which she encountered Dennis (via a medium) and communicated with Edward using a glass. More convincing is the encounter a chef reported in 1998. He reported seeing a shadow the size and shape of a large dog in the kitchen a few minutes before feeling a nip on his right thigh. He cried out, three nearby staff asking what was wrong, although they were disbelieving when he told them what he had seen and felt. The following day a heavily bruised bite mark appeared on his leg. | NULL  |    NULL |
| 51 | Overnight Construction                                 | Hay-on-Wye - Hay Castle                                                        | Legend                    | 1900-01-01 | The giant Moll Walbee, known also as Maud de Breos, built this castle in a single night! 
```

## 2.8.4 Londres

Lien pour scraper: [Londres](https://www.paranormaldatabase.com/london/lonpages/londdata.php)

```
select * from cas;
```

```
        | NULL  |    NULL |
| 53 | Woman in the Gutter                                    | E1 - Durward Street, Whitechapel (was Bucks Row)                                            | Haunting Manifestation    | 1888-01-01 | The general area is supposedly haunted by the sounds of the victims of Jack the Ripper, thought only in Durward Street has an apparition been observed; the glowing woman lays in the gutter, the location where Mary Ann Nichols was discovered (the Rippers first victim)                                                                                                                                                                                                                                                                                                                                                      | NULL  |    NULL |
| 54 | Smell of Death                                         | E1 - Former Bass Sales Office, Cephas Street                                                | Haunting Manifestation    | 1980-01-01 | Workers in this building, constructed on the site of an old Doctors office, reported cold spots and the smell of embalming fluid.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | NULL  |    NULL |
| 55 | Hanging Boy                                            | E1 - Former wicker cane warehouse (no longer standing), Durward Street                      | Unknown Ghost             | 1974-12-01 | A man at the warehouse, which was once a board school, encountered a ghostly young boy hanging from a cord attached to a hook on the ceiling.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | NULL  |    NULL |
| 56 | Annie Chapman                                          | E1 - Hanbury Street                                                                         | Haunting Manifestation    | 1930-10-01 | Annie Chapman was murdered by Jack the Ripper at 29 Hanbury Street, and it was said that she haunted the area where she was killed, her phantom accompanied by a man. On one occasion during the 1930s, a witness could hear her murder, but could see nothing at the site. A phantom, headless body in the area is also said to be Annie. 29 Hanbury Street has since been demolished.                                                                                
```

## 2.8.5 Other Regions

Lien pour scraper: [Other Regions](https://www.paranormaldatabase.com/europe/index.php)

```
 select * from cas;

```

```
       | NULL  |    NULL |
| 95 | Witch Tree                                             | Bladel (Netherlands, Noord-Brabant) - Tree in Ten Vorsel estate                             | Legend                    | 1850-01-01 | The tree was said to have been planted on the grave of a witch who also led a gang of local outlaws. The tree is said to create a strange atmosphere at night.                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | NULL  |    NULL |
| 96 | Kaatje                                                 | Blokzijl (Netherlands, Overijssel) - Bronze statue near the lock                            | Legend                    | 1850-01-01 | Kaatje was murdered in the seventeenth century, her killer never found and the motive for the crime never certain, although it was speculated that many of her recipes were stolen - such was the renown of her cooking. A bronze statue was erected of Kaatje and it is said if you stroke her head with your left hand it will bring luck, and with the right comes wealth. If one becomes greedy and tries both hands, a punishment is given out instead.                                                                                                                                                                     | NULL  |    NULL |
| 97 | Cornelis Theurisz                                      | Bobeldijk (Netherlands, Noord-Holland) - Unidentified house                                 | Post-Mortem Manifestation | 1616-06-25 | After setting off to sea, Cornelis Theuriszs wife found Cornelis in their home. Cornelis said that he had reached his ship late and that it had already sailed, so he had returned home. When the couple later lay down together, Theuriszs wife realised that he was ice cold. She called out for Gods help - Cornelis vanished and was never seen again                                                                                                                                                                                                                                                                        | NULL  |    NULL |
| 98 | John van Mijnsbergen                                   | Boxmeer (Netherlands, Noord-Brabant) - Area around Boxmeer Castle                           | Haunting Manifestation    | 1850-01-01 | John van Mijnsbergen, lord of the castle, was murdered by Joseph de Greef. Although the killer was known, he was never caught, and John still wanders around looking for him.                                                                               
```

## 2.8.6 Trouver toutes les catégories	

```
select * from elements;
```

```
+----+--------------------------+--------+---------+
| id | nom                      | parent | genre   |
+----+--------------------------+--------+---------+
|  1 | South East England       |   NULL | regions |
|  2 | Greater London           |   NULL | regions |
|  3 | East of England          |   NULL | regions |
|  4 | East Midlands            |   NULL | regions |
|  5 | West Midlands            |   NULL | regions |
|  6 | Northern Ireland         |   NULL | regions |
|  7 | Other Regions            |   NULL | regions |
|  8 | Republic of Ireland      |   NULL | regions |
|  9 | Wales                    |   NULL | regions |
| 10 | Scotland                 |   NULL | regions |
| 11 | North West England       |   NULL | regions |
| 12 | North East and Yorkshire |   NULL | regions |
| 13 | Recent Additions         |   NULL | reports |
| 14 | Reports:Places           |   NULL | reports |
| 15 | Contribute               |   NULL | reports |
| 16 | Calendar                 |   NULL | reports |
+----+--------------------------+--------+---------+
16 rows in set (0,00 sec)
```

## 2.8.7 Un cas avec une catégorie avec l'ID de la catégorie

```
SELECT cas.titre, cas_cat.id_cat 
FROM cas
JOIN cas_cat ON cas_cat.id_cas = cas.id
WHERE cas_cat.id_cat IN (
    SELECT id
    FROM categorie
    WHERE name LIKE 'South West England'
    OR parent IN (   
        SELECT id
        FROM categorie
        WHERE name LIKE 'South West England'
    )
);
```

```
+--------------+--------+
| titre        | id_cat |
+--------------+--------+
| Car Crossing |      2 |
+--------------+--------+
```

## 2.8.8 Nombre de cas avec seulement un mot pour les titres dans la base de données.	

```
SELECT COUNT(*) AS nombre_titres_avec_un_mot
FROM cas
WHERE LENGTH(titre) - LENGTH(REPLACE(titre, ' ', '')) + 1 = 1;

```

```
+---------------------------+
| nombre_titres_avec_un_mot |
+---------------------------+
|                      2854 |
+---------------------------+
```
