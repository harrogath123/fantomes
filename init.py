import requests
from bs4 import BeautifulSoup

# URL de la page à scraper
url = "https://www.paranormaldatabase.com/london/lonpages/londdata.php"

# Obtenir le contenu de la page
response = requests.get(url)
html_content = response.content

# Analyser le contenu HTML avec BeautifulSoup
soup = BeautifulSoup(html_content, 'html.parser')

# Extraire les titres, les lieux, les dates et les commentaires
events = soup.find_all('div', class_='w3-panel')

for event in events:
    # Extraire le titre
    title = event.find('h4').text.strip()

    # Extraire les informations de localisation, date et further comments
    location = None
    date_time = None
    further_comments = None
    
    # Rechercher les éléments pertinents
    info_elements = event.find_all('span', class_='w3-border-bottom')

    for info_element in info_elements:
        if 'Location' in info_element.text:
            location_span = info_element.find_next_sibling('span')
            location = location_span.text.strip() if location_span else None
        elif 'Date / Time' in info_element.text:
            date_time_span = info_element.find_next_sibling('span')
            date_time = date_time_span.text.strip() if date_time_span else None
        elif 'Further Comments' in info_element.text:
            further_comments_span = info_element.find_next_sibling('span')
            further_comments = further_comments_span.text.strip() if further_comments_span else None

    # Afficher les informations extraites
    print("Titre:", title)
    print("Location:", location)
    print("Date / Time:", date_time)
    print("Further Comments:", further_comments)

    # Afficher les autres éléments (p, br, a)
    other_elements = event.find_all(['p', 'br', 'a'])
    for elem in other_elements:
        print(elem)
    
    print("-" * 50)
