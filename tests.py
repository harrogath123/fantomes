from urllib.request import urlopen
from bs4 import BeautifulSoup

def get_page_content(url):
    return urlopen("https://www.paranormaldatabase.com"+url).read()


def get_list_css_class(contenu, class_css):
    pass


def get_category_name(contenu):
    pass

def cat_to_db(cat) -> int:
    pass



contenu_de_index = get_page_content('/index.html')
soup = BeautifulSoup(contenu_de_index, features="html.parser")

# liste sur la hp
# .w3-third
# .w3-half
# .w3-quarter

list_categorie = []

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_categorie.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_categorie.append(url)


# ....

for cat in list_categorie:
    contenu_de_ma_cat = get_page_content(cat)
    soup = BeautifulSoup(contenu_de_ma_cat, features="html.parser")
    nom_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    nom_cat.find("a").extract()
    nom_cat = nom_cat.getText()
    nom_cat = nom_cat.replace("> ", "")
    print(nom_cat)
    # est-ce qu'il existe dans la bdd ?
    # c'est quoi son id ?
    # etc...
    # etc...

import requests
from bs4 import BeautifulSoup

url = 'https://www.paranormaldatabase.com/regions/southeast.html'

# Définir un en-tête d'utilisateur pour simuler une requête de navigateur
headers = {'User-Agent': 'Mozilla/5.0'}

# Faire une demande à la page en utilisant les en-têtes d'utilisateur
response = requests.get(url, headers=headers)

# Vérifier si la demande s'est bien déroulée
if response.status_code == 200:
    # Analyser le contenu de la page avec BeautifulSoup
    soup = BeautifulSoup(response.content, 'html.parser')
    
    # Extraire les données nécessaires
    # Par exemple, pour extraire tous les liens sur la page :
    links = soup.find_all('a')
    
    # Afficher les liens
    for link in links:
        print(link.get('href'))
else:
    print('La requête a échoué avec le code de statut :', response.status_code)

from bs4 import BeautifulSoup

# HTML du paragraphe
html_content = """
<p><span class="w3-border-bottom">Location:</span> Antrim - Antrim Castle<br>
              <span class="w3-border-bottom">Type:</span> Haunting Manifestation<br>
              <span class="w3-border-bottom">Date / Time:</span> 31 May (reoccurring)<br>
              <span class="w3-border-bottom">Further Comments:</span> A coach pulled by a team of four horses which sunk into the pond drowning all on board is seen repeating the tragic accident once a year. In addition, a stone wolfhound which stands by the gateway is said to have once been flesh and blood - the dog gave a warning to an advancing sneak attack upon the castle before turning into rock. It now safeguards the Clotworthy family name on the condition it is never removed. Finally, the sound of heavy breathing can be heard coming from the ruined building.</p>
"""

# Analyse HTML avec BeautifulSoup
soup = BeautifulSoup(html_content, 'html.parser')

# Extraction des informations
location = soup.find('span', class_='w3-border-bottom', text='Location:').next_sibling.strip()
date_time = soup.find('span', class_='w3-border-bottom', text='Date / Time:').next_sibling.strip()
further_comments = soup.find('span', class_='w3-border-bottom', text='Further Comments:').next_sibling.strip()

print("Location:", location)
print("Date / Time:", date_time)
print("Further Comments:", further_comments)

import requests
from bs4 import BeautifulSoup

# URL de la page à scraper
url = "https://www.paranormaldatabase.com/ireland/antrim.php"

# Obtenir le contenu de la page
response = requests.get(url)
html_content = response.content

# Analyser le contenu HTML avec BeautifulSoup
soup = BeautifulSoup(html_content, 'html.parser')

# Extraire les titres, les lieux, les dates et les commentaires
events = soup.find_all('div', class_='w3-panel')

for event in events:
    # Extraire le titre
    title = event.find('h4').text.strip()

    # Extraire les informations de localisation, date et further comments
    location = None
    date_time = None
    further_comments = None
    
    # Rechercher les éléments pertinents
    info_elements = event.find_all('span', class_='w3-border-bottom')

    for info_element in info_elements:
        if 'Location' in info_element.text:
            location_span = info_element.find_next_sibling('span')
            location = location_span.text.strip() if location_span else None
        elif 'Date / Time' in info_element.text:
            date_time_span = info_element.find_next_sibling('span')
            date_time = date_time_span.text.strip() if date_time_span else None
        elif 'Further Comments' in info_element.text:
            further_comments_span = info_element.find_next_sibling('span')
            further_comments = further_comments_span.text.strip() if further_comments_span else None

    # Afficher les informations extraites
    print("Titre:", title)
    print("Location:", location)
    print("Date / Time:", date_time)
    print("Further Comments:", further_comments)

    # Afficher les autres éléments (p, br, a)
    other_elements = event.find_all(['p', 'br', 'a'])
    for elem in other_elements:
        print(elem)
    
    print("-" * 50)

import requests
from bs4 import BeautifulSoup

# URL de la page à scraper
url = "https://www.paranormaldatabase.com/ireland/antrim.php"

# Obtenir le contenu de la page
response = requests.get(url)
html_content = response.content

# Analyser le contenu HTML avec BeautifulSoup
soup = BeautifulSoup(html_content, 'html.parser')

# Extraire les titres, les lieux, les dates et les commentaires
events = soup.find_all('div', class_='w3-panel')

for event in events:
    # Extraire le titre
    title = event.find('h4').text.strip()

    # Extraire les informations de localisation, date et further comments
    location = None
    date_time = None
    further_comments = None
    
    # Rechercher les éléments pertinents
    info_elements = event.find_all('span', class_='w3-border-bottom')

    for info_element in info_elements:
        if 'Location' in info_element.text:
            location_span = info_element.find_next_sibling('span')
            location = location_span.text.strip() if location_span else None
        elif 'Date / Time' in info_element.text:
            date_time_span = info_element.find_next_sibling('span')
            date_time = date_time_span.text.strip() if date_time_span else None
        elif 'Further Comments' in info_element.text:
            further_comments_span = info_element.find_next_sibling('span')
            further_comments = further_comments_span.text.strip() if further_comments_span else None

    # Afficher les informations extraites
    print("Titre:", title)
    print("Location:", location)
    print("Date / Time:", date_time)
    print("Further Comments:", further_comments)

    # Afficher les autres éléments (p, br, a)
    other_elements = event.find_all(['p', 'br', 'a'])
    for elem in other_elements:
        print(elem)
    
    print("-" * 50)



















