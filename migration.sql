USE fantomes;

INSERT INTO elements (nom, parent, genre) VALUES ('South East England', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Greater London', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('East of England', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('East Midlands', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('West Midlands', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Northern Ireland', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Other Regions', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Republic of Ireland', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Wales', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('Scotland', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('North West England', NULL, 'regions');
INSERT INTO elements (nom, parent, genre) VALUES ('North East and Yorkshire', NULL, 'regions');

INSERT INTO elements (nom, parent, genre) VALUES ('Recent Additions', NULL, 'reports');
INSERT INTO elements (nom, parent, genre) VALUES ('Reports:Places', NULL, 'reports');
INSERT INTO elements (nom, parent, genre) VALUES ('Contribute', NULL, 'reports');
INSERT INTO elements (nom, parent, genre) VALUES ('Calendar', NULL, 'reports');
