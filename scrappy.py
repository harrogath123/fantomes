import pymysql
from urllib.request import urlopen
from bs4 import BeautifulSoup

connection = pymysql.connect(
    host='localhost',
    user='benjamin',
    password='coucou123',
    database='fantomes',
    cursorclass=pymysql.cursors.DictCursor
)

def execute_query(query, data=None):
    with connection.cursor() as cursor:
        cursor.execute(query, data)
    connection.commit()

def get_page_content(url):
    full_url = "https://www.paranormaldatabase.com" + url
    if not url.endswith("?"):
        full_url += "?"
    return urlopen(full_url).read()

html_home = get_page_content("/index.html")

list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url = lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url = lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url = lien.get('href')
    list_cat_home.append(url)

for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()

    list_sous_cat = []

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url = lien.get('href')
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for third in liste_halfs:
        lien = third.find('a')
        url = lien.get('href')
        list_sous_cat.append(url)

    for ss_cat in list_sous_cat:
        qs = "?"
        while qs:
            html_sous_cat = get_page_content(ss_cat + qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")

            list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")

            for cas in list_cas:
                
                titre_cas = cas.select_one("h4").get_text()

                p = cas.select_one("p:nth-last-child(1)")
                p_text = p.get_text().split("\n")

                location = p_text[0].replace("Location: ", "")

                cas_type = p_text[1].replace("Type: ", "")

                date_time = p_text[2].replace("Date / Time: ", "")
                further_comments = p_text[3].replace("Further Comments: ", "")

                try:
                    insert_query = "INSERT INTO cas (title, locations, elem_note, date, comments) VALUES (%s, %s, %s, %s, %s)"
                    execute_query(insert_query, (titre_cas, location, cas_type, date_time, further_comments))
                except pymysql.IntegrityError as e:
                    if e.args[0] == 1062:
                        print(f"Ignoring duplicate entry: {titre_cas}")
                    else:
                        raise  
                    
            nav_btn = soup.select(".w3-quarter.w3-container h5")
            qs = None
            for btn in nav_btn:
                if btn.get_text().find('next') > 0:
                    qs = btn.select_one("a").get("href")
                    break

connection.close()

    





