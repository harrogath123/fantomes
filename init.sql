DROP DATABASE IF EXISTS fantomes;
CREATE DATABASE IF NOT EXISTS fantomes;
USE fantomes;

CREATE TABLE cas (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255),
  locations VARCHAR(255),
  elem_note VARCHAR(255),
  date varchar (255),
  comments text,
  month DATE,
  weather BOOLEAN,
  UNIQUE (id),
  UNIQUE (title)
);

CREATE TABLE elements (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(100) NOT NULL,
  parent INT,
  genre ENUM ('reports', 'regions')
);

CREATE TABLE cas_elements (
  cas_id INT,
  elements_id INT,
  FOREIGN KEY (cas_id) REFERENCES cas(id),
  FOREIGN KEY (elements_id) REFERENCES elements(id)
);


